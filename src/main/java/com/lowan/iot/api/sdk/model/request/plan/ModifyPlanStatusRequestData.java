package com.lowan.iot.api.sdk.model.request.plan;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: ModifyPlanStatusRequestData.java
 * @author: ysp
 *
 * 修改计划状态请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Setter
@Getter
public class ModifyPlanStatusRequestData implements Serializable {

    @NotEmpty
    private String appid;

    @NotEmpty
    private String planid;

    @NotNull
    private Byte status;
}
