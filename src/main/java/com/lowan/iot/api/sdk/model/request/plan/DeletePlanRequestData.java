package com.lowan.iot.api.sdk.model.request.plan;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @title: DeletePlanRequestData.java
 * @author: ysp
 *
 * 删除计划请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class DeletePlanRequestData implements Serializable {

    @NotEmpty
    private String appid;

    @NotEmpty
    private String planid;
}
