package com.lowan.iot.api.sdk.model.request.groupinfo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @title: DeleteGroupInfoRequestData.java
 * @author: ysp
 *
 * 删除设备分组请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class DeleteGroupInfoRequestData implements Serializable {

    @NotEmpty
    private String groupCode;

    @NotEmpty
    private String tenantCode;
}
