package com.lowan.iot.api.sdk.openapi;

import com.lowan.apisign.SignatureRequest;
import com.lowan.iot.api.sdk.model.request.groupinfo.*;
import com.lowan.iot.api.sdk.util.OkHttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;

/**
 * @title: GroupInfoManager.java
 * @author: ysp
 * <p>
 * 设备分组管理
 */
@Slf4j
public class GroupInfoManager extends AbstractManager {

    private static final String URI = "http://api.lowaniot.com/openapi/groupinfo";

    /**
     * 创建设备分组
     *
     * @param data 请求参数
     */
    public static void createGroupInfo(@Validated CreateGroupInfoRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<CreateGroupInfoRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/create", signatureRequest);

        log.info("创建设备分组，result{}", result);

    }

    /**
     * 查询设备分组树结构
     *
     * @param data 请求参数
     */
    public static void queryGroupInfoTree(@Validated QueryGroupInfoTreeRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryGroupInfoTreeRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/query/tree", signatureRequest);

        log.info("查询设备分组树结构数据，result{}", result);

    }

    /**
     * 分页查询设备分组列表
     *
     * @param data 请求参数
     */
    public static void queryGroupInfoPage(@Validated QueryGroupInfoRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryGroupInfoRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/query/page", signatureRequest);

        log.info("分页查询设备分组列表，result{}", result);

    }

    /**
     * 查询设备分组详情
     *
     * @param data 请求参数
     */
    public static void queryGroupInfoDetail(@Validated QueryGroupInfoDetailRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryGroupInfoDetailRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/query/detail", signatureRequest);

        log.info("查询设备分组详情，result{}", result);

    }

    /**
     * 删除设备分组信息
     *
     * @param data 请求参数
     */
    public static void deleteGroupInfo(@Validated DeleteGroupInfoRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<DeleteGroupInfoRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/remove", signatureRequest);

        log.info("删除设备分组信息，result{}", result);

    }


}
