package com.lowan.iot.api.sdk.model.request.plan;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: QueryPlanTaskLogDetailRequestData.java
 * @author: ysp
 *
 * 查询计划批次下任务执行详情请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryPlanTaskLogDetailRequestData implements Serializable {

    @NotNull
    private Integer curPage;

    @NotNull
    private Integer pageSize;

    @NotEmpty
    private String appid;

    @NotEmpty
    private String planid;

    @NotNull
    private Integer batchId;

    private String taskId;

    private String devAddr;

    @NotEmpty
    private String startTime;

    @NotEmpty
    private String endTime;

    private String codes;
}
