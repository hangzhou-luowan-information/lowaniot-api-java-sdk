package com.lowan.iot.api.sdk.openapitest;

import com.lowan.iot.api.sdk.model.request.devgroup.CreateDevGroupRequestData;
import com.lowan.iot.api.sdk.model.request.devgroup.DeleteDevGroupRequestData;
import com.lowan.iot.api.sdk.model.request.devgroup.QueryDevGroupRequestData;
import com.lowan.iot.api.sdk.openapi.DevGroupManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @title: DevGroupProcess.java
 * @author: ysp
 * <p>
 * 设备与分组绑定相关操作过程
 */
public class DevGroupProcess {

    public static void main(String[] args) {
        DevGroupProcess devGroupProcess = new DevGroupProcess();
        devGroupProcess.DevGroupOne();
    }

    /**
     * 1.设备批量与分组绑定
     * 2.查询分组下绑定设备列表
     * 3.解绑分组下绑定设备
     * <p>
     * 响应内容参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
     */
    public void DevGroupOne() {

        // 批量创建设备与分组的绑定
        createDevGroupBindBatch();

        // 查询分组下绑定设备列表
        listDevGroup();

        // 批量解绑分组下绑定设备
        deleteDevGroupBatch();


    }

    /**
     * 批量创建设备与分组的绑定
     */
    private void createDevGroupBindBatch() {
        CreateDevGroupRequestData createDevGroupRequestData = new CreateDevGroupRequestData();
        createDevGroupRequestData.setAppid("c40vezvmrk");
        createDevGroupRequestData.setTenantCode("c3k2vnjncw");
        createDevGroupRequestData.setGroupCode("9dbd06a2330d47b08626");
        createDevGroupRequestData.setProductCode("c35mrio5j4");
        List<String> deviceIds = new ArrayList<>();
        deviceIds.add("b8c8kxiif7y8");
        deviceIds.add("b8c8qp2p6ku8");
        deviceIds.add("b8c8trcgbbpc");
        createDevGroupRequestData.setDeviceIds(deviceIds);

        DevGroupManager.createDevGroupBatch(createDevGroupRequestData);
    }

    /**
     * 查询分组下绑定设备列表
     */
    private void listDevGroup() {
        QueryDevGroupRequestData queryDevGroupRequestData = new QueryDevGroupRequestData();
        queryDevGroupRequestData.setTenantCode("c3k2vnjncw");
        queryDevGroupRequestData.setAppid("c40vezvmrk");
        // queryDevGroupRequestData.setGroupCode();  非必填
        // queryDevGroupRequestData.setDevGroupType();
        // queryDevGroupRequestData.setDeviceId();
        // queryDevGroupRequestData.setDeviceName();

        DevGroupManager.queryDevGroup(queryDevGroupRequestData);
    }

    /**
     * 批量解绑分组下绑定设备
     */
    private void deleteDevGroupBatch() {
        DeleteDevGroupRequestData deleteDevGroupRequestData = new DeleteDevGroupRequestData();
        deleteDevGroupRequestData.setTenantCode("c3k2vnjncw");
        deleteDevGroupRequestData.setGroupCode("9dbd06a2330d47b08626");
        List<String> deviceIds = new ArrayList<>();
        deviceIds.add("b8c8kxiif7y8");
        deviceIds.add("b8c8qp2p6ku8");
        deviceIds.add("b8c8trcgbbpc");
        deleteDevGroupRequestData.setDeviceIds(deviceIds);

        DevGroupManager.removeDevGroup(deleteDevGroupRequestData);
    }

}
