package com.lowan.iot.api.sdk.openapi;

import com.lowan.apisign.client.SignatureClient;

import java.io.Serializable;

/**
 * @title: AbstractManager.java
 * @author: ysp
 * <p>
 * 管理基类
 */
public abstract class AbstractManager implements Serializable {

    private static final String AAP_ID = "${appId}";

    private static final String SECRET_KEY = "${secretKey}";

    /**
     * 创建验签请求类客户端
     * <p>
     * appId：应用ID
     * secretKey：应用ID对应秘钥
     */
    static final SignatureClient signatureClient = SignatureClient.create(AAP_ID, SECRET_KEY);

}
