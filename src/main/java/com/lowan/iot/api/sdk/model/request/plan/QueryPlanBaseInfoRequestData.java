package com.lowan.iot.api.sdk.model.request.plan;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @title: QueryPlanBaseInfoRequestData.java
 * @author: ysp
 *
 * 查询计划基本信息配置
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryPlanBaseInfoRequestData implements Serializable {

    @NotEmpty
    private String appid;

    @NotEmpty
    private String planid;
}
