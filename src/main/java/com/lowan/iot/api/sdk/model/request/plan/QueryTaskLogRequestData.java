package com.lowan.iot.api.sdk.model.request.plan;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: QueryPlanTaskLogRequestData.java
 * @author: ysp
 *
 * 查询任务执行返回结果请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryTaskLogRequestData implements Serializable {

    @NotEmpty
    private String planid;

    @NotNull
    private Integer batchId;

    @NotEmpty
    private String devAddr;

    @NotNull
    private Long taskid;

    @NotEmpty
    private String appid;
}
