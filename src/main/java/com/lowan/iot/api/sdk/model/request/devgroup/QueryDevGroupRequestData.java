package com.lowan.iot.api.sdk.model.request.devgroup;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @title: QueryDevGroupRequestData.java
 * @author: ysp
 *
 * 查询分组下绑定设备请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryDevGroupRequestData implements Serializable {


    private String appid;

    private String groupCode;

    @NotEmpty
    private String tenantCode;

    private Byte devGroupType;

    private String deviceId;

    private String deviceName;
}
