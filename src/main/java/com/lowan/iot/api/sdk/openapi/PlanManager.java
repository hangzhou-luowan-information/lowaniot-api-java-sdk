package com.lowan.iot.api.sdk.openapi;

import com.lowan.apisign.SignatureRequest;
import com.lowan.iot.api.sdk.model.request.plan.*;
import com.lowan.iot.api.sdk.util.OkHttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;

/**
 * @title: PlanManager.java
 * @author: ysp
 * <p>
 * 计划管理相关操作
 */
@Slf4j
public class PlanManager extends AbstractManager {

    private static final String URI = "http://api.lowaniot.com/openapi/plan";

    /**
     * 创建计划
     *
     * @param data 请求数据
     */
    public static void createPlan(@Validated CreatePlanRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<CreatePlanRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/createplan", signatureRequest);

        log.info("创建计划，result{}", result);

    }

    /**
     * 修改计划
     *
     * @param data 请求数据
     */
    public static void modifyPlan(@Validated ModifyPlanRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<ModifyPlanRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/modifyplan", signatureRequest);

        log.info("修改计划，result{}", result);

    }

    /**
     * 删除计划
     *
     * @param data
     */
    public static void deletePlan(@Validated DeletePlanRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<DeletePlanRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/deleteplan", signatureRequest);

        log.info("删除计划，result{}", result);

    }

    /**
     * 修改计划状态
     *
     * @param data 请求参数
     */
    public static void modifyPlanStatus(@Validated ModifyPlanStatusRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<ModifyPlanStatusRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/modifyplanstatus", signatureRequest);

        log.info("修改计划状态，result{}", result);

    }

    /**
     * 分页查询计划列表
     *
     * @param data
     */
    public static void queryPlanPage(@Validated QueryPlanRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryPlanRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/listplaninfo", signatureRequest);

        log.info("分页查询计划列表，result{}", result);

    }

    /**
     * 查询计划基本信息配置
     *
     * @param data
     */
    public static void queryPlanBaseInfo(@Validated QueryPlanBaseInfoRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryPlanBaseInfoRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/getplaninfo", signatureRequest);

        log.info("查询计划基本信息配置，result{}", result);

    }

    /**
     * 分页查询计划批次列表
     *
     * @param data
     */
    public static void queryPlanBatchPage(@Validated QueryPlanBatchRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryPlanBatchRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/listplanbatch", signatureRequest);

        log.info("分页查询计划批次列表，result{}", result);

    }

    /**
     * 分页查询计划批次下任务执行详情
     *
     * @param data
     */
    public static void queryPlanTaskLogDetail(@Validated QueryPlanTaskLogDetailRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryPlanTaskLogDetailRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/listtasklog", signatureRequest);

        log.info("分页查询计划批次下任务执行详情，result{}", result);

    }

    /**
     * 查询任务执行结果集返回明细
     *
     * @param data
     */
    public static void queryTaskLog(@Validated QueryTaskLogRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryTaskLogRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/gettasklog", signatureRequest);

        log.info("查询任务执行结果集返回明细，result{}", result);

    }

    /**
     * 查询产品物模型
     *
     * @param data
     */
    public static void queryObjectModel(@Validated QueryObjectModelRequestData data) {


        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryObjectModelRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/getobjectmodel", signatureRequest);

        log.info("查询任务执行结果集返回明细，result{}", result);


    }

}
