package com.lowan.iot.api.sdk.model.request.devgroup;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @title: CreateDevGroupRequestData.java
 * @author: ysp
 *
 * 设备与分组绑定请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class CreateDevGroupRequestData implements Serializable {

    @NotEmpty
    private String appid;

    @NotEmpty
    private String groupCode;

    @NotBlank
    private List<String> deviceIds;

    @NotEmpty
    private String productCode;

    @NotEmpty
    private String tenantCode;

}
