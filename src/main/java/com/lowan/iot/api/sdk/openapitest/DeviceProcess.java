package com.lowan.iot.api.sdk.openapitest;

import com.alibaba.fastjson.JSONObject;
import com.lowan.iot.api.sdk.model.request.device.CreateDeviceRequestData;
import com.lowan.iot.api.sdk.model.request.device.DeleteDeviceRequestData;
import com.lowan.iot.api.sdk.model.request.device.QueryDeviceRequestData;
import com.lowan.iot.api.sdk.openapi.DeviceManager;

/**
 * @title: DeviceProcess.java
 * @author: ysp
 * <p>
 * 设备操作过程
 */
public class DeviceProcess {

    public static void main(String[] args) {

        DeviceProcess deviceProcess = new DeviceProcess();
        deviceProcess.deviceOne();
    }

    /**
     * 1.创建设备
     * 2.查询设备信息列表
     * 3.删除设备
     * <p>
     * 响应内容参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
     */
    public void deviceOne() {

        // 创建一个设备
        createDevice();

        // 查询设备列表
        listDevice();

        // 删除设备
        deleteDevice();

    }

    /**
     * 创建设备
     */
    private void createDevice() {

        CreateDeviceRequestData createDeviceRequestData = new CreateDeviceRequestData();
        // createDeviceRequestData.setDeviceId("b8c8trcgbbpc");  更新时必填
        createDeviceRequestData.setAppid("c40vezvmrk");
        createDeviceRequestData.setTenantCode("c3k2vnjncw");
        createDeviceRequestData.setProductCode("c35mrio5j4");
        createDeviceRequestData.setDevAddr("20220101****9905");
        createDeviceRequestData.setDeviceName("ysp test device 05");
        createDeviceRequestData.setDevGroupType((byte) 1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mode", "server");
        jsonObject.put("ip", "iot.lowan.com");
        jsonObject.put("port", "8080");
        createDeviceRequestData.setExtendInfo(jsonObject);

        DeviceManager.createDevice(createDeviceRequestData);
    }

    /**
     * 查询设备列表
     */
    private void listDevice() {
        QueryDeviceRequestData queryDeviceRequestData = new QueryDeviceRequestData();
        queryDeviceRequestData.setCurPage(1);  // 设置为零表示不分页
        queryDeviceRequestData.setPageSize(10);
        queryDeviceRequestData.setAppid("c40vezvmrk");
        queryDeviceRequestData.setTenantCode("c3k2vnjncw");
        queryDeviceRequestData.setProductCode("c35mrio5j4");
        // queryDeviceRequestData.setDevAddr("20220101****9905");  非必填
        // queryDeviceRequestData.setDeviceName();
        // queryDeviceRequestData.setDevGroupType();
        // queryDeviceRequestData.setCommunAddr();

        DeviceManager.queryDevicePage(queryDeviceRequestData);
    }

    /**
     * 删除设备
     */
    private void deleteDevice() {
        DeleteDeviceRequestData deleteDeviceRequestData = new DeleteDeviceRequestData();
        deleteDeviceRequestData.setDeviceId("b8e6weycn2f4");

        DeviceManager.deleteDevice(deleteDeviceRequestData);
    }
}
