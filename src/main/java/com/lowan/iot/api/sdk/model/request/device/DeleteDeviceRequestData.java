package com.lowan.iot.api.sdk.model.request.device;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @title: DeleteDeviceRequestData.java
 * @author: ysp
 *
 * 删除设备信息请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class DeleteDeviceRequestData implements Serializable {

    @NotEmpty
    private String deviceId;
}
