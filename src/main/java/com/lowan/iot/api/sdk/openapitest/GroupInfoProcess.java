package com.lowan.iot.api.sdk.openapitest;

import com.lowan.iot.api.sdk.model.request.groupinfo.*;
import com.lowan.iot.api.sdk.openapi.GroupInfoManager;

/**
 * @title: GroupInfoProcess.java
 * @author: ysp
 * <p>
 * 设备分组操作过程
 */
public class GroupInfoProcess {

    public static void main(String[] args) {

        GroupInfoProcess groupInfoProcess = new GroupInfoProcess();
        groupInfoProcess.groupInfoOne();

    }


    /**
     * 1.创建设备分组
     * 2.查询设备分组（不包含子分组）
     * 3.查询设备分组树结构（包含子分组）
     * 4.查询设备分组详情
     * 5.删除设备分组
     * <p>
     * 响应内容参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
     */
    public void groupInfoOne() {

        // 创建设备分组
        createGroupInfo();

        // 分页查询设备分组
        listGroupInfo();

        // 查询设备分组树结构
        treeGroupInfo();

        // 查询设备分组详情
        getGroupInfoDetail();

        // 删除设备分组
        deleteGroupInfo();
    }


    /**
     * 创建设备分组
     */
    private void createGroupInfo() {
        CreateGroupInfoRequestData createGroupInfoRequestData = new CreateGroupInfoRequestData();
        createGroupInfoRequestData.setAppid("c40vezvmrk");
        createGroupInfoRequestData.setGroupName("lowan test group 01");
        createGroupInfoRequestData.setTenantCode("c3k2vnjncw");
        createGroupInfoRequestData.setParentGroupCode("0");  // 如果创建一级分组，此项填 0
        // createGroupInfoRequestData.setLocationConvert();  // 标准json格式 非必填

        GroupInfoManager.createGroupInfo(createGroupInfoRequestData);
    }

    /**
     * 查询设备分组列表
     */
    private void listGroupInfo() {
        QueryGroupInfoRequestData queryGroupInfoRequestData = new QueryGroupInfoRequestData();
        queryGroupInfoRequestData.setCurPage(1);
        queryGroupInfoRequestData.setPageSize(10);
        queryGroupInfoRequestData.setTenantCode("c3k2vnjncw");
        queryGroupInfoRequestData.setParentGroupCode("0");
        // queryGroupInfoRequestData.setAppid(); 非必填项
        // queryGroupInfoRequestData.setGroupName(); 非必填项

        GroupInfoManager.queryGroupInfoPage(queryGroupInfoRequestData);
    }

    /**
     * 查询设备分组树结构
     */
    private void treeGroupInfo() {
        QueryGroupInfoTreeRequestData queryGroupInfoTreeRequestData = new QueryGroupInfoTreeRequestData();
        queryGroupInfoTreeRequestData.setTenantCode("c3k2vnjncw");
        queryGroupInfoTreeRequestData.setParentGroupCode("0");
        // queryGroupInfoTreeRequestData.setAppid(); 非必填项

        GroupInfoManager.queryGroupInfoTree(queryGroupInfoTreeRequestData);
    }

    /**
     * 查询设备分组详情
     */
    private void getGroupInfoDetail() {
        QueryGroupInfoDetailRequestData queryGroupInfoDetailRequestData = new QueryGroupInfoDetailRequestData();
        queryGroupInfoDetailRequestData.setTenantCode("c3k2vnjncw");
        queryGroupInfoDetailRequestData.setGroupCode("9401db0466d148ce9a8c");

        GroupInfoManager.queryGroupInfoDetail(queryGroupInfoDetailRequestData);
    }

    /**
     * 删除设备分组
     */
    private void deleteGroupInfo() {
        DeleteGroupInfoRequestData deleteGroupInfoRequestData = new DeleteGroupInfoRequestData();
        deleteGroupInfoRequestData.setTenantCode("c3k2vnjncw");
        deleteGroupInfoRequestData.setGroupCode("9401db0466d148ce9a8c");

        GroupInfoManager.deleteGroupInfo(deleteGroupInfoRequestData);
    }

}
