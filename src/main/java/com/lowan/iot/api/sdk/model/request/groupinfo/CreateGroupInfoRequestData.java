package com.lowan.iot.api.sdk.model.request.groupinfo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @title: CreateGroupInfoRequestData.java
 * @author: ysp
 *
 * 创建设备分组请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class CreateGroupInfoRequestData implements Serializable {

    @NotEmpty
    private String appid;

    @NotEmpty
    private String groupName;

    @NotEmpty
    private String tenantCode;

    @NotEmpty
    private String parentGroupCode;

    private String locationConvert;
}
