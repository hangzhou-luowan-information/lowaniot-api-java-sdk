package com.lowan.iot.api.sdk.model.request.device;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: QueryDeviceRequestData.java
 * @author: ysp
 *
 * 查询设备信息请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Data
public class QueryDeviceRequestData implements Serializable {

    @NotNull
    @Range(min = 1)
    private Integer curPage;

    @NotNull
    @Range(min = 0, max = 10000)
    private Integer pageSize;

    private String appid;

    private String tenantCode;

    private String productCode;

    private String devAddr;

    private String deviceName;

    private Byte  devGroupType;

    private String communAddr;
}
