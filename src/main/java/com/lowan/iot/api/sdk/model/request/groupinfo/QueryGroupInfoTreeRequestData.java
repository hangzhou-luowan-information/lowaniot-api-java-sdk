package com.lowan.iot.api.sdk.model.request.groupinfo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @title: QueryGroupInfoTreeRequestData.java
 * @author: ysp
 * <p>
 * 设备分组树结构查询请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryGroupInfoTreeRequestData implements Serializable {

    private String appid;

    @NotEmpty
    private String tenantCode;

    @NotEmpty
    private String parentGroupCode;
}
