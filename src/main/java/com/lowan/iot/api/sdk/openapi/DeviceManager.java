package com.lowan.iot.api.sdk.openapi;

import com.lowan.apisign.SignatureRequest;
import com.lowan.iot.api.sdk.model.request.device.CreateDeviceRequestData;
import com.lowan.iot.api.sdk.model.request.device.DeleteDeviceRequestData;
import com.lowan.iot.api.sdk.model.request.device.QueryDeviceRequestData;
import com.lowan.iot.api.sdk.util.OkHttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;


/**
 * @title: DeviceManager.java
 * @author: ysp
 * <p>
 * 设备管理
 */
@Slf4j
public class DeviceManager extends AbstractManager {

    private static final String URI = "http://api.lowaniot.com/openapi/devinfo";

    /**
     * 创建、更新设备
     *
     * @param data 请求参数
     */
    public static void createDevice(@Validated CreateDeviceRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/save/update", signatureRequest);

        log.info("创建、更新设备，result: {}", result);

    }

    /**
     * 分页查询设备信息
     *
     * @param data 请求参数
     */
    public static void queryDevicePage(@Validated QueryDeviceRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/page", signatureRequest);

        log.info("查询设备信息列表，result{}", result);

    }

    /**
     * 删除设备信息
     *
     * @param data 请求参数
     */
    public static void deleteDevice(@Validated DeleteDeviceRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/delete", signatureRequest);

        log.info("删除设备信息，result{}", result);

    }


}
