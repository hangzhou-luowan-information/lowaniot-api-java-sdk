package com.lowan.iot.api.sdk.openapi;

import com.lowan.apisign.SignatureRequest;
import com.lowan.iot.api.sdk.model.request.devgroup.CreateDevGroupRequestData;
import com.lowan.iot.api.sdk.model.request.devgroup.DeleteDevGroupRequestData;
import com.lowan.iot.api.sdk.model.request.devgroup.QueryDevGroupRequestData;
import com.lowan.iot.api.sdk.model.request.groupinfo.*;
import com.lowan.iot.api.sdk.util.OkHttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;

/**
 * @title: DevGroupManager.java
 * @author: ysp
 * <p>
 * 设备分组绑定相关操作
 */
@Slf4j
public class DevGroupManager extends AbstractManager {

    private static final String URI = "http://api.lowaniot.com/openapi/devgroup";

    /**
     * 分组批量绑定设备
     *
     * @param data 请求参数
     */
    public static void createDevGroupBatch(@Validated CreateDevGroupRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<CreateDevGroupRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/bind/batch", signatureRequest);

        log.info("分组批量绑定设备，result{}", result);

    }

    /**
     * 查询分组下绑定全部设备
     *
     * @param data 请求参数
     */
    public static void queryDevGroup(@Validated QueryDevGroupRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<QueryDevGroupRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/query/binddev/list", signatureRequest);

        log.info("查询分组下绑定全部设备，result{}", result);

    }

    /**
     * 解绑分组下绑定设备
     *
     * @param data
     */
    public static void removeDevGroup(@Validated DeleteDevGroupRequestData data) {

        // 通过签名请求客户端初始化请求体
        SignatureRequest<DeleteDevGroupRequestData> signatureRequest = signatureClient.signatureRequest(data);

        // 请求lowanIot openApi 提供接口
        String result = OkHttpUtil.post(URI + "/remove/binddev", signatureRequest);

        log.info("解绑分组下绑定设备，result{}", result);

    }


}
