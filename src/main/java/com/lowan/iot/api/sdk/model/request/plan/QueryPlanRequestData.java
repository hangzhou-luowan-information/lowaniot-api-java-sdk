package com.lowan.iot.api.sdk.model.request.plan;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: QueryPlanRequestData.java
 * @author: ysp
 *
 * 查询计划列表请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryPlanRequestData implements Serializable {

    @NotNull
    private Integer curPage;

    @NotNull
    private Integer pageSize;

    @NotEmpty
    private String tenantCode;

    private String appid;

    private String productCode;

    private String planid;

    private String planName;

    private Byte planStatus;
}
