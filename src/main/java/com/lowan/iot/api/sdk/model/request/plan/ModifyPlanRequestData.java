package com.lowan.iot.api.sdk.model.request.plan;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: ModifyPlanRequestData.java
 * @author: ysp
 *
 * 修改计划请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class ModifyPlanRequestData implements Serializable {

    @NotEmpty
    private String planid;

    @NotEmpty
    private String appid;

    @NotEmpty
    private String tenantCode;

    @NotEmpty
    private String productCode;

    @NotEmpty
    private String planName;

    @NotNull
    private Byte pri;

    @NotNull
    private Integer timeout;

    @NotNull
    private JSONObject devs;

    @NotNull
    private JSONObject crontab;

    @NotNull
    private JSONArray orders;
}
