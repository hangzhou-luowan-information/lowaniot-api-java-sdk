package com.lowan.iot.api.sdk.openapitest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lowan.iot.api.sdk.model.request.plan.*;
import com.lowan.iot.api.sdk.openapi.PlanManager;

/**
 * @title: PlanProcess.java
 * @author: ysp
 * <p>
 * 计划管理操作过程
 */
public class PlanProcess {

    public static void main(String[] args) {

        PlanProcess planProcess = new PlanProcess();
        planProcess.planOne();
    }

    /**
     * 1.创建计划
     * 2.分页查询计划列表
     * 3.查询计划的基本信息配置
     * 4.分页查询计划批次列表
     * 5.分页查询某批次下所有任务执行详情
     * 6.查询任务执行结果集返回明细
     * 7.获取产品物模型
     * 8.修改计划
     * 9.修改计划状态
     * 10.删除计划
     * <p>
     * 响应内容参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
     */
    public void planOne() {

        // 创建计划
        createPlan();

        // 分页查询计划列表
        listPlanPage();

        // 查询计划的基本信息配置
        getPlanBaseInfo();

        // 分页查询计划批次列表
        listPlanBatchPage();

        // 分页查询某批次下所有任务执行详情
        listPlanTaskLogDetailPage();

        // 查询任务执行结果集返回明细
        getTaskLog();

        // 获取产品物模型
        getObjectModel();

        // 修改计划
        modifyPlan();

        // 修改计划状态
        modifyPlanStatus();

        // 删除计划
        deletePlan();
    }


    /**
     * 创建计划
     */
    private void createPlan() {
        CreatePlanRequestData createPlanRequestData = new CreatePlanRequestData();
        createPlanRequestData.setAppid("c40vezvmrk");
        createPlanRequestData.setTenantCode("c3k2vnjncw");
        createPlanRequestData.setProductCode("c35mrio5j4");
        createPlanRequestData.setPlanName("lowan test plan 01");
        createPlanRequestData.setPri((byte) 1);
        createPlanRequestData.setTimeout(30);

        JSONObject params = new JSONObject();
        params.put("data", "abcdefghijklmn");

        String[] devList = {"b8c9rxffqjgg"};

        JSONObject devs = new JSONObject();
        devs.put("itemType", (byte) 0);
        devs.put("devList", devList);
        devs.put("devParams", params);
        createPlanRequestData.setDevs(devs);

        JSONObject repairConf = new JSONObject();
        repairConf.put("repairCount", 3);
        repairConf.put("repairIntervalTime", 1);
        repairConf.put("repairTimeUnit", 0);

        JSONObject crontab = new JSONObject();
        crontab.put("starttime", 0);
        crontab.put("freq", 1);
        crontab.put("count", 1);
        crontab.put("unit", 0);
        crontab.put("repairConf", repairConf);
        createPlanRequestData.setCrontab(crontab);

        JSONObject item1 = new JSONObject();
        item1.put("type", "readProperty");
        item1.put("dataKey", "3132");
        item1.put("params", params);

        JSONArray orders = new JSONArray();
        orders.add(item1);
        createPlanRequestData.setOrders(orders);

        PlanManager.createPlan(createPlanRequestData);
    }

    /**
     * 分页查询计划列表
     */
    private void listPlanPage() {
        QueryPlanRequestData queryPlanRequestData = new QueryPlanRequestData();
        queryPlanRequestData.setCurPage(1);
        queryPlanRequestData.setPageSize(10);
        queryPlanRequestData.setTenantCode("c3k2vnjncw");

        // queryPlanRequestData.setAppid(); 非必填
        // queryPlanRequestData.setProductCode();
        // queryPlanRequestData.setPlanid();
        // queryPlanRequestData.setPlanName();
        // queryPlanRequestData.setPlanStatus();

        PlanManager.queryPlanPage(queryPlanRequestData);
    }

    /**
     * 查询计划基础信息配置
     */
    private void getPlanBaseInfo() {
        QueryPlanBaseInfoRequestData queryPlanBaseInfoRequestData = new QueryPlanBaseInfoRequestData();
        queryPlanBaseInfoRequestData.setAppid("c40vezvmrk");
        queryPlanBaseInfoRequestData.setPlanid("c40vezvmrk_2022010610422618400098");

        PlanManager.queryPlanBaseInfo(queryPlanBaseInfoRequestData);
    }

    /**
     * 分页查询计划批次列表
     */
    private void listPlanBatchPage() {
        QueryPlanBatchRequestData queryPlanBatchRequestData = new QueryPlanBatchRequestData();
        queryPlanBatchRequestData.setCurPage(1);
        queryPlanBatchRequestData.setPageSize(10);
        queryPlanBatchRequestData.setAppid("c40vezvmrk");

        // queryPlanBatchRequestData.setPlanid();  非必填
        // queryPlanBatchRequestData.setPlanName();
        // queryPlanBatchRequestData.setStartTime();
        // queryPlanBatchRequestData.setEndTime();

        PlanManager.queryPlanBatchPage(queryPlanBatchRequestData);
    }

    /**
     * 分页查询计划批次下所有的任务执行详情
     */
    private void listPlanTaskLogDetailPage() {
        QueryPlanTaskLogDetailRequestData queryPlanTaskLogDetailRequestData = new QueryPlanTaskLogDetailRequestData();
        queryPlanTaskLogDetailRequestData.setCurPage(1);
        queryPlanTaskLogDetailRequestData.setPageSize(10);
        queryPlanTaskLogDetailRequestData.setAppid("c40vezvmrk");
        queryPlanTaskLogDetailRequestData.setPlanid("c40vezvmrk_2022010610422618400098");
        queryPlanTaskLogDetailRequestData.setBatchId(1);
        queryPlanTaskLogDetailRequestData.setStartTime("2021-01-01 10:10:10");  //yyyy-MM-dd HH:mm:ss
        queryPlanTaskLogDetailRequestData.setEndTime("2022-01-06 10:10:10");  //yyyy-MM-dd HH:mm:ss

        // queryPlanTaskLogDetailRequestData.setTaskId();  非必填
        // queryPlanTaskLogDetailRequestData.setDevAddr();
        // queryPlanTaskLogDetailRequestData.setCodes();

        PlanManager.queryPlanTaskLogDetail(queryPlanTaskLogDetailRequestData);
    }

    /**
     * 查询任务执行结果集返回明细
     */
    private void getTaskLog() {
        QueryTaskLogRequestData queryTaskLogRequestData = new QueryTaskLogRequestData();
        queryTaskLogRequestData.setPlanid("c40vezvmrk_2022010610475642200098");
        queryTaskLogRequestData.setBatchId(1);
        queryTaskLogRequestData.setDevAddr("b8c9rxffqjgg");
        queryTaskLogRequestData.setTaskid(210000000000000009L);
        queryTaskLogRequestData.setAppid("c40vezvmrk");

        PlanManager.queryTaskLog(queryTaskLogRequestData);
    }

    /**
     * 获取产品物模型
     */
    private void getObjectModel() {
        QueryObjectModelRequestData queryObjectModelRequestData = new QueryObjectModelRequestData();
        queryObjectModelRequestData.setProductCode("4y6gfqfy0w");

        PlanManager.queryObjectModel(queryObjectModelRequestData);
    }

    /**
     * 更改计划
     */
    private void modifyPlan() {
        ModifyPlanRequestData modifyPlanRequestData = new ModifyPlanRequestData();
        modifyPlanRequestData.setPlanid("c40vezvmrk_2022010610475642200098");
        modifyPlanRequestData.setAppid("c40vezvmrk");
        modifyPlanRequestData.setTenantCode("c3k2vnjncw");
        modifyPlanRequestData.setProductCode("c35mrio5j4");
        modifyPlanRequestData.setPlanName("lowan test plan 01");
        modifyPlanRequestData.setPri((byte) 1);
        modifyPlanRequestData.setTimeout(30);

        JSONObject params = new JSONObject();
        params.put("data", "abcdefghijklmn");

        String[] devList = {"b8c9rxffqjgg"};

        JSONObject devs = new JSONObject();
        devs.put("itemType", (byte) 0);
        devs.put("devList", devList);
        devs.put("devParams", params);
        modifyPlanRequestData.setDevs(devs);

        JSONObject repairConf = new JSONObject();
        repairConf.put("repairCount", 3);
        repairConf.put("repairIntervalTime", 1);
        repairConf.put("repairTimeUnit", 0);

        JSONObject crontab = new JSONObject();
        crontab.put("starttime", 0);
        crontab.put("freq", 1);
        crontab.put("count", 1);
        crontab.put("unit", 0);
        crontab.put("repairConf", repairConf);
        modifyPlanRequestData.setCrontab(crontab);

        JSONObject item1 = new JSONObject();
        item1.put("type", "readProperty");
        item1.put("dataKey", "3132");
        item1.put("params", params);

        JSONArray orders = new JSONArray();
        orders.add(item1);
        modifyPlanRequestData.setOrders(orders);

        PlanManager.modifyPlan(modifyPlanRequestData);
    }

    /**
     * 修改计划状态
     */
    private void modifyPlanStatus() {
        ModifyPlanStatusRequestData modifyPlanStatusRequestData = new ModifyPlanStatusRequestData();
        modifyPlanStatusRequestData.setAppid("c40vezvmrk");
        modifyPlanStatusRequestData.setPlanid("c40vezvmrk_2022010610422618400098");
        modifyPlanStatusRequestData.setStatus((byte) 0);

        PlanManager.modifyPlanStatus(modifyPlanStatusRequestData);
    }

    /**
     * 删除计划
     */
    private void deletePlan() {
        DeletePlanRequestData deletePlanRequestData = new DeletePlanRequestData();
        deletePlanRequestData.setAppid("c40vezvmrk");
        deletePlanRequestData.setPlanid("c40vezvmrk_2022010611105265500098");

        PlanManager.deletePlan(deletePlanRequestData);
    }
}
