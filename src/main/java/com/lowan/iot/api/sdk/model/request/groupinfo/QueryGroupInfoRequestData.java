package com.lowan.iot.api.sdk.model.request.groupinfo;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: QueryGroupInfoRequestData.java
 * @author: ysp
 * <p>
 * 设备分组查询请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryGroupInfoRequestData implements Serializable {

    @NotNull
    @Range(min = 1)
    private Integer curPage;

    @NotNull
    @Range(min = 0, max = 10000)
    private Integer pageSize;

    @NotEmpty
    private String tenantCode;

    @NotEmpty
    private String parentGroupCode;

    private String appid;

    private String groupName;
}
