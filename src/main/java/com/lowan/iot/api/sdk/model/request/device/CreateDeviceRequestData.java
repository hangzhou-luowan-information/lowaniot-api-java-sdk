package com.lowan.iot.api.sdk.model.request.device;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: DeviceDTO.java
 * @author: ysp
 *
 * 创建设备请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Data
public class CreateDeviceRequestData implements Serializable {

    // 更新时有值
    private String deviceId;

    @NotEmpty
    private String appid;

    @NotEmpty
    private String tenantCode;

    @NotEmpty
    private String productCode;

    @NotEmpty
    private String devAddr;

    @NotEmpty
    private String deviceName;

    @NotNull
    private Byte devGroupType;

    private String communAddr;

    private JSONObject location;

    @NotNull
    private JSONObject extendInfo;
}
