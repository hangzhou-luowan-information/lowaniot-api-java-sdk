package com.lowan.iot.api.sdk.model.request.devgroup;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @title: DeleteDevGroupRequestData.java
 * @author: ysp
 *
 * 解绑分组下设备请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class DeleteDevGroupRequestData implements Serializable {

    @NotEmpty
    private String groupCode;

    @NotBlank
    private List<String> deviceIds;

    @NotEmpty
    private String tenantCode;
}
