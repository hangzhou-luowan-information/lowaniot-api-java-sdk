package com.lowan.iot.api.sdk.model.request.plan;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @title: QueryPlanBatchRequestData.java
 * @author: ysp
 *
 * 分页查询计划批次列表请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryPlanBatchRequestData implements Serializable {

    @NotNull
    private Integer curPage;

    @NotNull
    private Integer pageSize;

    @NotEmpty
    private String appid;

    private String planid;

    private String planName;

    private String startTime;

    private String endTime;
}
