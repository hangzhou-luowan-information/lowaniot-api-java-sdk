package com.lowan.iot.api.sdk.model.request.plan;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @title: QueryObjectModelRequestData.java
 * @author: ysp
 *
 * 查询产品物模型请求数据封装类
 * 具体字段含义参照 @see https://doc.lowaniot.com/#/platform-access/open-api.html
 */
@Getter
@Setter
public class QueryObjectModelRequestData implements Serializable {

    @NotEmpty
    private String productCode;
}
