# lowaniot-api-java-sdk

罗万物联网平台OpenAPI展示代码

#### 介绍
**Lowan-IoT-Links 物联网平台， 满足各类设备物联入网需求，集成设备管理，数据安全通信及消息订阅等能力的一体化平台。**

使用物联网平台实现设备完整的通信链接，lowaniot提供一套关于应用、设备、操作管理等OpenAPI。帮助快速开发应用，满足场景业务需求，并且可以通过动态下发指令至设备端，实现对设备的远程控制。

#### 软件架构
​	JDK 1.8


#### 安装教程

安装LowanIot 提供Jar包，添加到项目本地依赖。

- **`api-sign-core`**

- **`api-sign-client`**

#### 使用说明

1.  要使用 LowanIot SDK for Java ，您需要申请一对`APPID`和`SecretKey`。 请联系您的系统管理员。
2.  平台需要对API调用方进行资源权限校验，使用API时，http body需要统一使用`SignatureRequest`请求类。
3.  所有`SignatureRequest`所携带数据封装类需实现`Serializable`接口

**注**：另可参见 [Lowan-IoT-Links 全链路物联网平台](https://doc.lowaniot.com/#/platform-access/open-api.html)

